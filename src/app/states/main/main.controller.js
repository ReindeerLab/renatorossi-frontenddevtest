(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($state, $scope) {
    var _this = this;

    _this.allStates = $state.get();

    $scope.availableStates = _this.allStates.filter(function (obj) {
        return obj.url && obj.url != '^'
    });
  }
})();

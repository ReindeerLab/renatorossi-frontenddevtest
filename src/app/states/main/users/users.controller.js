(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .controller('UsersController', UsersController);

  /** @ngInject */
  function UsersController($http, $q) {

    var _this = this;

    _this.userList = null;

    _this.getData = function() {
      return $q(function(resolve, reject){
       $http.get('http://localhost:8000/users').then(function (response) {
         _this.userList = response.data;
         resolve(response.data);
       }, function () {
         reject();
       })
      });
    };

    _this.getData();

    _this.sendFormData = function (newEntry) {
      $http.post('http://localhost:8000/user', newEntry).then(function (response) {
        _this.getData();
      });
    };

  }
})();

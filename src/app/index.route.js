(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        abstract: true,
        templateUrl: 'app/states/main/main.html',
        controller: 'MainController'
      })
      .state('main.home', {
        url: '/',
        menutitle: 'home',
        templateUrl: 'app/states/main/home/home.html',
        controller: 'HomeController',
        controllerAs: 'home'
      })
      .state('main.users', {
        url: '/users',
        menutitle: 'users',
        templateUrl: 'app/states/main/users/users.html',
        controller: 'UsersController',
        controllerAs: 'users'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
